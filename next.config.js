/** @type {import('next').NextConfig} */
const nextConfig = {
  output: 'standalone',
  async redirects() {
    return [
      {
        source: '/',
        destination: '/vids',
        permanent: true,
      },
    ]
  },
}

module.exports = nextConfig
