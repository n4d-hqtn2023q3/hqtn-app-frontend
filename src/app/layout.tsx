import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import ogImage from './assets/og-image.png'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: "AI-Powered Video Translation and Localization | AI-Tech",
  description:
    "Unlock a world of viewership with cutting-edge AI technology.Reach a global audience with ease. Expand your reach, captivate viewers worldwide, and unlock new opportunities for growth.",
  openGraph: {
    title: "AI-Powered Video Translation and Localization",
    description:
      "Unlock a world of viewership with cutting-edge AI technology.Reach a global audience with ease. Expand your reach, captivate viewers worldwide, and unlock new opportunities for growth.",
    images: [`https://b-v3.space${ogImage.src}`],
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
  )
}
