import { type StaticImageData } from "next/image";
import { type ReactNode } from "react";
import avatarSrc from "../assets/transcription-author-placeholder.png";

interface Data {
  avatarName: string;
  avatarSrc: StaticImageData;
  timeElapsed: string;
  timeTotal: string;
  transcriptionOriginal: string;
  transcriptionProcessed: ReactNode;
  progressPercentage?: number;
  active?: boolean;
}

const d: Data[] = [
  {
    avatarName: "Chloe",
    avatarSrc,
    timeElapsed: "00:00",
    timeTotal: "00:04",
    transcriptionOriginal: "Love betrayed, hearts shattered.",
    transcriptionProcessed: "Liebe verraten, Herzen zerschmettert.",
  },
  {
    avatarName: "Chloe",
    avatarSrc,
    timeElapsed: "00:12",
    timeTotal: "00:16",
    progressPercentage: 82,
    transcriptionOriginal:
      "Once upon a time, in a magical forest, a curious rabbit discovered a hidden treasure. With excitement, it hopped back to its burrow, sharing the secret with its forest friends, and they lived happily ever after.",
    transcriptionProcessed: (
      <>
        Es war einmal in einem magischen Wald, als ein neugieriger Hase einen
        verborgenen Schatz entdeckte. Aufgeregt hüpfte es zu seinem Bau zurück
        und teilte das{" "}
        <span className="py-[2px] px-1 text-white bg-[#090E35] rounded-sm">
          Geheimnis
        </span>{" "}
        mit seinen Waldfreunden, und sie lebten glücklich bis ans Ende ihrer
        Tage.
      </>
    ),
    active: true,
  },
  {
    avatarName: "Chloe",
    avatarSrc,
    timeElapsed: "00:16",
    timeTotal: "01:30",
    transcriptionOriginal:
      "In a vast digital realm, a server hummed tirelessly, processing vast amounts of data. One fateful day, a malicious virus infiltrated the system, spreading like wildfire through interconnected nodes. Panic ensued as firewalls were breached, and the network teetered on the brink of collapse. But amidst chaos, the cybersecurity team mounted a fierce counterattack, deploying advanced encryption algorithms and digital defenses. Their valiant efforts paid off as the virus was eradicated, restoring stability and safeguarding the digital ecosystem.",
    transcriptionProcessed:
      "In einem riesigen digitalen Reich summte ein Server unermüdlich und verarbeitete riesige Datenmengen. Eines schicksalhaften Tages infiltrierte ein bösartiger Virus das System und verbreitete sich wie ein Lauffeuer über miteinander verbundene Knoten. Panik folgte, als Firewalls durchbrochen wurden und das Netzwerk kurz vor dem Zusammenbruch stand. Inmitten des Chaos startete das Cybersicherheitsteam einen heftigen Gegenangriff und setzte fortschrittliche Verschlüsselungsalgorithmen und digitale Abwehrmaßnahmen ein. Ihre tapferen Bemühungen zahlten sich aus, als das Virus ausgerottet, die Stabilität wiederhergestellt und das digitale Ökosystem geschützt wurde.",
  },
];

export default d;
