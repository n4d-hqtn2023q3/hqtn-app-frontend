import cn from "clsx";
import Image, { StaticImageData } from "next/image";
import { type FC, type PropsWithChildren } from "react";

interface TranscriptionSentenceProps extends PropsWithChildren {
  className?: string;
  active?: boolean;
  avatarName: string;
  avatarSrc?: StaticImageData;
  timeElapsed?: string;
  timeTotal?: string;
  progressPercentage?: number;
}

const TranscriptionSentence: FC<TranscriptionSentenceProps> = ({
  className,
  active = false,
  avatarSrc,
  avatarName,
  children,
  timeTotal,
  timeElapsed,
  progressPercentage,
}) => {
  return (
    <div className={cn(className, "flex flex-col gap-4")}>
      <div className="pt-4 px-4 flex items-center justify-between">
        <div className="flex items-center gap-4">
          {avatarSrc && (
            <Image
              src={avatarSrc}
              alt="avatar"
              width="42"
              height="42"
              className="rounded-full"
            />
          )}
          <p className="text-white text-2xl leading-6">{avatarName}</p>
        </div>

        {timeElapsed && timeTotal && (
          <p className={active ? "text-[#FFFFFF]" : "text-[#FFFFFF80]"}>
            {timeElapsed} /{" "}
            <span className={active ? "text-[#2F69D7]" : "text-[#2F69D780]"}>
              {timeTotal}
            </span>
          </p>
        )}
      </div>
      <p
        className={cn("p-4 text-justify relative overflow-hidden", {
          "bg-[#2F69D7] rounded-b-lg rounded-tr-lg": active,
        })}
      >
        {children}
        {progressPercentage && (
          <span
            className="absolute bottom-0 left-0 h-[3px] bg-white"
            style={{ width: `${progressPercentage}%` }}
          />
        )}
      </p>
    </div>
  );
};

export default TranscriptionSentence;
