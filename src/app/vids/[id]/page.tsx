"use client";

import cn from "clsx";

import TranscriptionSentence from "./components/TranscriptionSentence";
import transcription from "./const/transcription";
import s from "./vid.module.css";
import { useEffect, useRef, useState } from "react";

export default function VidPage({ params }: { params: { id: string } }) {
  const ref = useRef<HTMLVideoElement>(null);
  const [playing, setPlaying] = useState(false);
  useEffect(() => {
    if (ref.current && ref.current !== null) {
      ref.current.addEventListener("play", () => {
        setPlaying(true);
      });
      ref.current.addEventListener("pause", () => {
        setPlaying(false);
      });
    }
  }, []);

  return (
    <div className={cn(s.container, "flex w-full min-h-full items-start")}>
      <div
        className={cn(
          s.player,
          "min-w-[539px] w-[539px] bg-[#070707] sticky top-0 overflow-auto rounded-lg flex flex-col"
        )}
      >
        <video src="/placeholder-video.mp4" ref={ref} className="px-6 pt-6" />

        <p className="px-6 flex justify-between mt-1">
          <span className="text-white">
            <span className="text-[#2F69D7]">00:00:11</span> / 00:03:45
          </span>

          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
          >
            <path
              d="M7 7V10M7 7H10M7 7L10.5 10.5M7 18V15M7 18H10M7 18L10.5 14.5M18 7L15 7M18 7V10M18 7L14.5 10.5M18 18H15M18 18V15M18 18L14.5 14.5"
              stroke="white"
              stroke-width="1.4"
              stroke-linecap="round"
              stroke-linejoin="round"
            />
          </svg>
        </p>

        <div className="flex w-full items-center justify-center gap-2">
          <button className="text-[#2F69D7] hover:text-[#2F69D780]">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="32"
              height="32"
              viewBox="0 0 32 32"
              fill="none"
            >
              <path
                d="M25 20.8114C25 21.6752 24.0668 22.2167 23.3169 21.7882L16.2094 17.7267C15.4536 17.2949 15.4536 16.2051 16.2094 15.7732L23.3169 11.7118C24.0668 11.2832 25 11.8248 25 12.6886V20.8114Z"
                stroke="currentColor"
                stroke-width="1.4"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M15.25 20.8114C15.25 21.6752 14.3168 22.2167 13.5669 21.7882L6.45936 17.7267C5.70357 17.2949 5.70357 16.2051 6.45936 15.7732L13.5669 11.7118C14.3168 11.2832 15.25 11.8248 15.25 12.6886L15.25 20.8114Z"
                stroke="currentColor"
                stroke-width="1.4"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </button>

          <button
            className="rounded-full bg-[#2F69D7] hover:bg-[#2F69D780] w-[54px] h-[54px] flex items-center justify-center"
            onClick={() => {
              playing ? ref.current?.pause() : ref.current?.play();
            }}
          >
            {playing ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="36"
                height="36"
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M14.5556 7L14.5556 17M9 7V17"
                  stroke="#090E35"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="54"
                height="54"
                viewBox="0 0 54 54"
                fill="none"
              >
                <path
                  d="M37 27L22.75 36.5263L22.75 17.4737L37 27Z"
                  fill="#090E35"
                />
              </svg>
            )}
          </button>

          <button className="text-[#2F69D7] hover:text-[#2F69D780]">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="32"
              height="32"
              viewBox="0 0 32 32"
              fill="none"
            >
              <path
                d="M6.99999 11.1886C6.99999 10.3248 7.93316 9.78326 8.68315 10.2118L15.7906 14.2733C16.5464 14.7051 16.5464 15.7949 15.7906 16.2268L8.68315 20.2882C7.93316 20.7168 6.99999 20.1752 6.99999 19.3114L6.99999 11.1886Z"
                stroke="currentColor"
                stroke-width="1.4"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M16.75 11.1886C16.75 10.3248 17.6832 9.78326 18.4332 10.2118L25.5406 14.2733C26.2964 14.7051 26.2964 15.7949 25.5406 16.2268L18.4331 20.2882C17.6832 20.7168 16.75 20.1752 16.75 19.3114L16.75 11.1886Z"
                stroke="currentColor"
                stroke-width="1.4"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </button>
        </div>

        <div className="p-6 flex-grow bg-[#131314] mt-6 flex flex-col items-center justify-end">
          <button className="text-white font-semibold w-[170px] flex items-center justify-center h-[48px] rounded-md bg-[#2F69D7] hover:bg-[#2F69D780]">
            Download
          </button>
          <p className="text-xs text-[#FFFFFF40] mt-3 text-center max-w-[350px]">
            Get legal requirements, best practices, industry trends, and video
            accessibility tips straight to your inbox.
          </p>
        </div>
      </div>

      <div className="flex-grow bg-[#131314] border-l border-[#2F69D780]">
        {transcription.map((v) => (
          <div
            className={cn("p-6 flex", { "bg-[#D9D9D90D]": v.active })}
            key={v.timeTotal}
          >
            <TranscriptionSentence
              avatarName={v.avatarName}
              avatarSrc={v.avatarSrc}
              className="w-1/2"
            >
              {v.transcriptionOriginal}
            </TranscriptionSentence>

            <TranscriptionSentence
              avatarName={v.avatarName}
              avatarSrc={v.avatarSrc}
              className="w-1/2"
              active={v.active}
              timeElapsed={v.timeElapsed}
              timeTotal={v.timeTotal}
              progressPercentage={v.progressPercentage}
            >
              {v.transcriptionProcessed}
            </TranscriptionSentence>
          </div>
        ))}
      </div>
    </div>
  );
}
