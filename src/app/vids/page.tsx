import Link from "next/link";
import srcVideoThumbnail from "./video-thumbnail-placeholder.png";
import Image from "next/image";

export default function VidsPage() {
  return (
    <div className="p-6 flex flex-wrap w-full">
      <Link
        href="/vids/1"
        className="flex gap gap-4 p-4 bg-white bg-opacity-5 border border-transparent hover:border-white hover:border-opacity-20 rounded"
      >
        <Image src={srcVideoThumbnail} alt="video thumbnail" />
        <div className="flex flex-col justify-center">
          <p className="text-white leading-4">Super Interesting Interview</p>
          <p className="mt-1 text-xs text-[#2F69D7BF] leading-3">
            1 min 30 seconds
          </p>
        </div>
      </Link>
    </div>
  );
}
