import cn from "clsx";
import { type FC } from "react";
import Image from "next/image";
import srcAvatar from "./avatar-placeholder.png";
import { logotype } from "./logotype";

const Footer: FC<{ className?: string }> = ({ className }) => {
  return (
    <footer
      className={cn(
        "bg-[#161819] flex justify-between items-center p-6 border-t border-[#FFFFFF0D]",
        className
      )}
    >
      {logotype}
      <div className="flex gap-4 items-center">
        <div>
          <p className="text-white leading-4">Joseph Strong</p>
          <p className="mt-1 text-xs text-[#2F69D7BF] leading-3">
            j.strong@example.com
          </p>
        </div>
        <Image
          src={srcAvatar}
          alt="user avatar"
          className="border-2 border-[#2F69D7] rounded-full w-[48px]"
        />
      </div>
    </footer>
  );
};

export default Footer;
