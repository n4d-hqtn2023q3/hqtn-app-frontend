import cn from "clsx";
import { type FC } from "react";

const PlayButton: FC<{ className?: string; playing?: boolean }> = ({
  className,
  playing = false,
}) => {
  return playing ? (
    <button
      className={cn(
        className,
        "rounded-full bg-[#2F69D740] hover:bg-[#2F69D780] w-[36px] h-[36px] flex items-center justify-center"
      )}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
      >
        <path
          d="M14.5556 7L14.5556 17M9 7V17"
          stroke="#2F69D7"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
      </svg>
    </button>
  ) : (
    <button
      className={cn(
        className,
        "rounded-full w-[36px] h-[36px] bg-white bg-opacity-5 hover:bg-opacity-10"
      )}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="36"
        height="36"
        viewBox="0 0 36 36"
        fill="none"
      >
        <path d="M24 17.5L15 23.1292L15 11.8708L24 17.5Z" fill="#2F69D7" />
      </svg>
    </button>
  );
};

export default PlayButton;
