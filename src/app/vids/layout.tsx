"use client";

import cn from "clsx";
import { usePathname } from "next/navigation";
import Link from "next/link";
import Image from "next/image";
import Footer from "./components/Footer";
import PlayButton from './components/PlayButton';
import srcVideoThumbnail from "./video-thumbnail-placeholder.png";
import s from "./vids.module.css";


export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const pathname = usePathname();

  return (
    <div className={cn(s.container)}>
      {pathname === "/vids" ? (
        <header
          className={cn(
            s.header,
            "bg-[#161819] flex items-center p-6 border-b border-white border-opacity-5"
          )}
        >
          <p className="m-0 text-white">Drafts</p>
        </header>
      ) : (
        <header
          className={cn(
            s.header,
            "bg-[#161819] flex border-b border-white border-opacity-5"
          )}
        >
          <div className="flex min-w-[540px] w-[540px] p-4 justify-between items-center border-r border-[#2F69D780]">
            <div className="flex gap-4 items-center">
              <Image
                src={srcVideoThumbnail}
                alt="video thumbnail"
                className="w-[48px]"
              />
              <div className="flex flex-col justify-center">
                <p className="text-white leading-4">
                  <span className="text-[#2F69D780]">Drafts /</span> Super
                  Interesting Interview
                </p>
                <p className="mt-1">
                  <svg
                    width="64"
                    height="16"
                    viewBox="0 0 64 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M9.49519 6.375L9.32212 10.875M6.92788 10.875L6.75481 6.375M11.7388 4.77028C11.9098 4.79611 12.0802 4.82373 12.25 4.85315M11.7388 4.77028L11.2049 11.7113C11.1598 12.2974 10.6711 12.75 10.0832 12.75H6.16679C5.57893 12.75 5.09019 12.2974 5.0451 11.7113L4.51118 4.77028M11.7388 4.77028C11.1656 4.68372 10.5858 4.61743 10 4.57216M4 4.85315C4.16979 4.82373 4.34019 4.79611 4.51118 4.77028M4.51118 4.77028C5.08439 4.68372 5.66425 4.61743 6.25 4.57216M10 4.57216V4.11409C10 3.52441 9.54467 3.03212 8.95529 3.01326C8.67961 3.00444 8.40282 3 8.125 3C7.84718 3 7.57039 3.00444 7.29471 3.01326C6.70533 3.03212 6.25 3.52441 6.25 4.11409V4.57216M10 4.57216C9.38128 4.52435 8.75598 4.5 8.125 4.5C7.49402 4.5 6.86872 4.52435 6.25 4.57216"
                      stroke="white"
                      stroke-opacity="0.75"
                      stroke-width="0.7"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M39.0935 4.47003C39.1387 4.19879 39.3734 4 39.6484 4H40.9453C41.2203 4 41.455 4.19879 41.5002 4.47003L41.6069 5.11055C41.6381 5.29757 41.7631 5.45336 41.929 5.54518C41.9661 5.56571 42.0027 5.58692 42.0389 5.60879C42.2015 5.70692 42.3991 5.73743 42.5769 5.67083L43.1853 5.44289C43.4428 5.34642 43.7323 5.45025 43.8698 5.68839L44.5183 6.81161C44.6558 7.04974 44.601 7.35237 44.3887 7.52713L43.8865 7.94054C43.7403 8.06094 43.6678 8.24686 43.6713 8.43626C43.6717 8.45746 43.6719 8.47871 43.6719 8.5C43.6719 8.52129 43.6717 8.54253 43.6713 8.56373C43.6678 8.75313 43.7403 8.93905 43.8865 9.05946L44.3887 9.47286C44.601 9.64763 44.6558 9.95025 44.5183 10.1884L43.8698 11.3116C43.7323 11.5497 43.4428 11.6536 43.1853 11.5571L42.5769 11.3292C42.3991 11.2626 42.2015 11.2931 42.039 11.3912C42.0027 11.4131 41.9661 11.4343 41.929 11.4548C41.7631 11.5466 41.6381 11.7024 41.6069 11.8895L41.5002 12.53C41.455 12.8012 41.2203 13 40.9453 13H39.6484C39.3734 13 39.1387 12.8012 39.0935 12.53L38.9868 11.8895C38.9556 11.7024 38.8306 11.5466 38.6647 11.4548C38.6276 11.4343 38.591 11.4131 38.5548 11.3912C38.3922 11.2931 38.1946 11.2626 38.0168 11.3292L37.4084 11.5571C37.1509 11.6536 36.8614 11.5497 36.7239 11.3116L36.0754 10.1884C35.9379 9.95026 35.9927 9.64763 36.205 9.47287L36.7072 9.05946C36.8535 8.93906 36.926 8.75314 36.9224 8.56374C36.9221 8.54254 36.9219 8.52129 36.9219 8.5C36.9219 8.47871 36.9221 8.45747 36.9224 8.43627C36.926 8.24687 36.8535 8.06095 36.7072 7.94055L36.205 7.52714C35.9927 7.35237 35.9379 7.04975 36.0754 6.81161L36.7239 5.68839C36.8614 5.45026 37.1509 5.34643 37.4084 5.4429L38.0168 5.67083C38.1946 5.73744 38.3922 5.70692 38.5548 5.60879C38.591 5.58692 38.6276 5.56571 38.6647 5.54518C38.8306 5.45336 38.9556 5.29757 38.9868 5.11055L39.0935 4.47003Z"
                      stroke="white"
                      stroke-opacity="0.75"
                      stroke-width="0.7"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M41.7967 8.49996C41.7967 9.32839 41.1252 9.99996 40.2967 9.99996C39.4683 9.99996 38.7967 9.32839 38.7967 8.49996C38.7967 7.67153 39.4683 6.99996 40.2967 6.99996C41.1252 6.99996 41.7967 7.67153 41.7967 8.49996Z"
                      stroke="white"
                      stroke-opacity="0.75"
                      stroke-width="0.7"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M21.0893 6.75L19 7.875L21.0893 9M21.0893 6.75L23.875 8.25L26.6607 6.75M21.0893 6.75L19 5.625L23.875 3L28.75 5.625L26.6607 6.75M26.6607 6.75L28.75 7.875L26.6607 9M26.6607 9L28.75 10.125L23.875 12.75L19 10.125L21.0893 9M26.6607 9L23.875 10.5L21.0893 9"
                      stroke="white"
                      stroke-opacity="0.75"
                      stroke-width="0.7"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M53.75 8.375C53.75 8.58211 53.5821 8.75 53.375 8.75C53.1679 8.75 53 8.58211 53 8.375C53 8.16789 53.1679 8 53.375 8C53.5821 8 53.75 8.16789 53.75 8.375Z"
                      stroke="white"
                      stroke-opacity="0.75"
                      stroke-width="0.7"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M56.75 8.375C56.75 8.58211 56.5821 8.75 56.375 8.75C56.1679 8.75 56 8.58211 56 8.375C56 8.16789 56.1679 8 56.375 8C56.5821 8 56.75 8.16789 56.75 8.375Z"
                      stroke="white"
                      stroke-opacity="0.75"
                      stroke-width="0.7"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M59.75 8.375C59.75 8.58211 59.5821 8.75 59.375 8.75C59.1679 8.75 59 8.58211 59 8.375C59 8.16789 59.1679 8 59.375 8C59.5821 8 59.75 8.16789 59.75 8.375Z"
                      stroke="white"
                      stroke-opacity="0.75"
                      stroke-width="0.7"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                  </svg>
                </p>
              </div>
            </div>

            <Link href="/vids">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M6 18L18 6M6 6L18 18"
                  stroke="#2F69D7"
                  stroke-width="2"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>
            </Link>
          </div>
          <div className="flex w-1/3 p-4 justify-between items-center border-r border-white border-opacity-5">
            <div>
              <p className="text-white text-2xl leading-6">American English</p>
              <p className="text-white opacity-50 text-xs leading-3 mt-1">
                Source Language
              </p>
            </div>

            <PlayButton />
          </div>
          <div className="flex w-1/3 p-4 justify-between items-center">
            <div>
              <p className="text-white text-2xl leading-6">German</p>
              <p className="text-white opacity-50 text-xs leading-3 mt-1">
                Target Language
              </p>
            </div>

            <PlayButton playing />
          </div>
        </header>
      )}

      <section className={cn(s.content)}>{children}</section>

      <Footer className={s.footer} />
    </div>
  );
}
